module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		express: {
			all: {
				options: {
					port: 1338,
					hostname: "0.0.0.0",
					bases: ["build"],
				}
			}
		},
		watch: {
			all: {
				files: 'index.html',
				options: {
					livereload: true
				}
			}
		},
		open: {
			all: {
				// Gets the port from the connect configuration
				path: 'http://localhost:<%= express.all.options.port%>/index.html'
			}
		},
		copy: {
			html: {
				files: [
					{cwd: 'html/', expand: true, src: '**/*', dest: 'build/'}
				]
			},
			assets: {
				files: [
					//Custom App
					{ cwd: 'assets/', expand: true, src: '**/*', dest: 'build/assets/'}
				]
			},
			tmp: {
				files: [
					//Custom App
					{ cwd: 'tmp/', expand: true, src: '**/*', dest: 'build/'}
				]
			}
		},
		concat: {
			libs: {
				src: [	
					'bower/**/jquery.js',
					'bower/**/underscore.js',
					'bower/**/ring.js',
					'bower/**/phaser.js'
				],
				dest: 'tmp/libs.js'
			},
			app: {
				src: [	
					'src/**/AbstractViewState.js',
					'src/**/*',
					'!src/Init.js'
				],
				dest: 'tmp/main.js'
			},
			init: {
				src: [	
					'tmp/main.js',
					'src/Init.js'
				],
				dest: 'tmp/main.js'
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			dist: {
				files: {
				  'tmp/<%= pkg.name %>.js': ['tmp/<%= pkg.name %>.js'],
				  'tmp/libs.js': ['tmp/libs.js']
				}
			}
		},
		bower: {
			install: {
				options: {
					targetDir: './bower',
					layout: 'byType',
					install: true,
					verbose: false,
					cleanTargetDir: false,
					cleanBowerDir: true,
					bowerOptions: {}
				}
			}
		},
		clean: {
			options: { force: true },
			tmp: ["tmp"],
			build: ["build"]
		},
		'json-minify': {
		  build: {
			files: 'tmp/assets/**/*.js'
		  }
		}
	});
  
	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-open');
	grunt.loadNpmTasks('grunt-contrib-livereload');
	grunt.loadNpmTasks('grunt-regarde');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-json-minify');
	grunt.loadNpmTasks('grunt-express');

	//tasks
	grunt.registerTask('install', ['bower']);
	grunt.registerTask('launch', [ 'express:all', 'open', 'watch' ]);
	grunt.registerTask('build', [ 'clean', 'concat:libs', 'concat:app', 'concat:init', 'copy:assets', 'copy:html', 'copy:tmp', 'clean:tmp' ]);
	grunt.registerTask('default', ['build', 'launch']);

};