Build instructions:

1) Download and install: nodeJs(http://nodejs.org/) y git 

	I recommend you to use chocolatey. For that: 
	Install chocolatey packet manager: https://chocolatey.org/

	Restart console, for refresh environment variables if it's necessary.
	After that now you can install git and nodejs in this way: 

		>choco install git
		>choco install nodejs

2) Install Grunt in global mode
	>npm install -g grunt-cli

3) Project structure:
	src// source code and resources
	build// project build

4)From root folder, run in a console:
	>npm install (*requires nodejs and git installed)

5)Now you could run these commands:
	>grunt install // this install libs dependencies
	>grunt build //build the project
	>grunt launch (Launch server with the current build)
	>grunt (Build and launch the project)

Franco Barletta
francoagusto@hotmail.com
francoabarletta@gmail.com