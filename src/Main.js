var Main = ring.create({
	
	CANVAS_WRAPPER_ID: "wrapper",
	ASSET_PATH: "assets/assets.json",
	
	constructor: function() {
		this.initialize();
	},
	
	initialize: function () {

		console.log("Init");
		Model = new GAME.model.Model();
		
		Model.currentBalance = Model.config.initBalance;
		Model.render = new CORE.render.Render();
		Model.render.eventDispatcher.suscribe(Model.render.START_APP_MESSAGE, this._handleRenderReady, this);
		Model.render.initialize(Model.config.stageWidth, Model.config.stageHeight, Model.config.ASSET_PATH, Model.config.CANVAS_WRAPPER_ID);

		Model.stateManager = new CORE.state.StateManager();		
	},
	
	_handleRenderReady: function() {
		Model.stateManager.changeState(GAME.views.IntroState);
	}
});