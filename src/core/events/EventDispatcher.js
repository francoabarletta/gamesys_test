var CORE = CORE || {};
CORE.events = CORE.events || {};

//set Messenger as singleton
CORE.events.EventDispatcher = ring.create({
	_messages: null,

	constructor: function()
	{
		this._messages = {};
	},

	_getList: function (params) {
		var register;
		for(var ob in this._messages) { 
			register = this._messages[ob];
		} 
	},


	_registerKey: function(key) {
		if (this._messages[key] == null) {
			this._messages[key] = new CORE.events.KeyRegister();
		}
	},

	_unRegisterKey: function(key) {
		if (!this._messages[key])
		{
			console.log(this.toString() + "::unRegisterKey , Key " + key + " is not registered");				
			return false;
		}
		delete this._messages[key];
		return true;
	},

	suscribe: function(key, callback, scope) {
		this._registerKey(key);

		if (callback == null)
		{
			console.log(this.toString() + "::suscribe " + "callback is null");				
			return;
		}
		var register = this._messages[key];
		if (register == null)
		{
			console.log(this.toString() +"::suscribe " + "the key doesn't exists");				
			return;
		}
		register.add(callback, scope);
	},

	unsuscribe: function (key,  callback) {
		var register = this._messages[key];
		if (register)
		{
			register.remove(callback);
		}
		else
		{
			console.log(this.toString() + "::unsuscribe " + "Key " + key + " is not registered");
		}			
	},

	sendMessage: function (key,  message) {
		console.log("SEND MESSAGE :: " + key);
		var register = this._messages[key];
		if (register)
		{
			register.sendMessage(message);
		}
		else
		{
			console.log(this.toString() + "::sendMessage " + "The key: '" + key + "' is not registered");
		}			
	},
	
	toString: function()
	{
		return "CORE.events.EventDispatcher";
	}
});


