var CORE = CORE || {};
CORE.events = CORE.events || {};

CORE.events.KeyRegister = ring.create({

	constructor: function()
	{
		this._callbacks = new Array(); 
	},

	add: function(callback, scope) {
		this._callbacks.push({callback: callback, callbackScope: scope});
	},

	remove: function(callback) {

		if (this._callbacks != null && this._callbacks.length > 0) {
			var index = this.findWithAttribute(this._callbacks, "callback", callback);
			this._callbacks.splice(index, 1);
		}
	},

	sendMessage: function (message) {
		if (this._callbacks.length > 0) {
			//if it has elements
			var callbackObject;
			for (var i = 0; i < this._callbacks.length; i++)
			{
				callbackObject = this._callbacks[i];
				//call only with params
				callbackObject.callback.call(callbackObject.callbackScope, message);

			}
		} else {
			console.log("There are not callbacks regristred yet.");
		}			
	},
	
	toString: function()
	{
		return "CORE.eventDispatcher.KeyRegister";
	},

	findWithAttribute: function(array, attr, value) {
	    for(var i = 0; i < array.length; i += 1) {
	        if(array[i][attr] === value) {
	            return i;
	        }
	    }
	}
});