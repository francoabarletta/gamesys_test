var CORE = CORE || {};
CORE.render = CORE.render || {};

CORE.render.AssetFactory = ring.create({

    SPRITE: "spritesheet",
    IMAGE: "image",
    BITMAP_DATA: "bitmapData",
    AUDIO: "audio",

    _core: null,
    _functionByType: {},

    constructor: function(core) {
        this._core = core;
        this._functionByType[this.SPRITE] = this.createSprite;
        this._functionByType[this.IMAGE] = this.createImage;
        this._functionByType[this.BITMAP_DATA] = this.createBitmapData;
        this._functionByType[this.AUDIO] = this.createAudio;
    },
    
    //args: type, ...
	//return Asset
    createAsset: function(type) {
        var functionToApply = this._functionByType[type];
        var args = [];
        //get args from index 1
        for (var i = 1; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        return functionToApply.apply(this, args);
    },

    /*Elements Factory*/
	createText: function(style) {
       return new Phaser.Text(this._core, 0, 0, "", style)
	},

    //light weight visual object
	createImage: function(key, frame) {
       return new Phaser.Image(this._core, 0, 0, key, frame);
	},

	createSprite: function(key, frame) {
       return new Phaser.Sprite(this._core, 0, 0, key, frame);
    },

    createButton: function(key, frames, callbacks) {
	    var button = new Phaser.Button(this._core, 0, 0, key, null, callbacks.scope, frames.over, frames.out, frames.down, frames.up);
       
		button.inputEnabled = true;
		
		if (callbacks.over != null) {
			button.events.onInputOver.add(callbacks.over, callbacks.scope);
		}

		if (callbacks.out != null) {
			button.events.onInputOut.add(callbacks.out, callbacks.scope);
		}

		if (callbacks.down != null) {
			button.events.onInputDown.add(callbacks.down, callbacks.scope);
		}

		if (callbacks.up != null) {
			button.events.onInputUp.add(callbacks.up, callbacks.scope);
		}
	    return button;
	},

    createAudio: function(key) {
        return this._core.add.audio(key);
    },

    createBitmapData: function(key, element) {
        var bitmapData = new Phaser.BitmapData(this._core, key);
        return bitmapData;
    },

    createGroup: function() {
        return new Phaser.Group(this._core);
    },	

    createGraphics: function() {
	    return new Phaser.Graphics(this._core, 0, 0);
	},
 
});