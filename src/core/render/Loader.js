var CORE = CORE || {};
CORE.render = CORE.render || {};

CORE.render.Loader = ring.create({
	//events
    PRELOAD_COMPLETE: "preloadComplete",
    PRELOAD_START: "preloadStart",
    PRELOAD_PROGRESS: "preloadProgress",

    eventDispatcher: null,
    preloaderPath: null,
    _jsonAssets: null,
    _core: null,

    constructor: function() {
    },

	setCore: function(core) {
		this._core = core;
	},
	
    existPreload: function() {
        return (this.preloaderPath != null);
    },

    setPreloadPath: function(path) {
        this.preloaderPath = path;
    },

    startPreload: function(preloaderPath) {
        this._jsonAssets = preloaderPath || this.preloaderPath;

        this._core.load.onLoadStart.add(this._onLoadStart, this);
        this._core.load.onFileComplete.add(this._onFileComplete, this);
        this._core.load.onLoadComplete.add(this._onLoadComplete, this);

        this._core.load.pack("assets", this._jsonAssets, null, this);
        this._core.load.start();
    },

    getCache: function() {
        return this._core.cache;
    },

    toString: function() {
        return "CORE.render.Loader";
    },
	
    _onLoadStart: function() {
        console.log("Loader::onLoadStart");
        this.eventDispatcher.sendMessage(this.PRELOAD_START, {});
    },

    _onFileComplete: function(progress, cacheKey, success, loadedFiles, totalFiles) {
        console.log("Loader::onFileComplete " + this._core.load.progress + "%");
        this.eventDispatcher.sendMessage(this.PRELOAD_PROGRESS, this._core.load.progress);
    },

    _onLoadComplete: function() {-
        this.eventDispatcher.sendMessage(this.PRELOAD_COMPLETE, {});
    }
});