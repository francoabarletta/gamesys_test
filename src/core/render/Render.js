var CORE = CORE || {};
CORE.render = CORE.render || {};

CORE.render.Render = ring.create({
	
	//messages
    START_APP_MESSAGE: 'START_APP_MESSAGE',
   
	//consts
    SHOW_ALL: Phaser.ScaleManager.SHOW_ALL,
    NO_SCALE: Phaser.ScaleManager.NO_SCALE,
	DEFAULT_CANVAS_ID: 'canvasContainer',

	eventDispatcher: null,
	factory: null,
	loader: null,

	_engine: null,
	
	constructor: function() {
		this.eventDispatcher = new CORE.events.EventDispatcher();
	},

	initialize: function(width, height, preloaderPath, canvasContainer) {
		//init Phaser Engine
		this.loader = new CORE.render.Loader();
		this.loader.eventDispatcher = this.eventDispatcher;
		this.loader.setPreloadPath(preloaderPath);
		
		this._engine = new Phaser.Game(width, height, Phaser.AUTO, canvasContainer || this.DEFAULT_CANVAS_ID, { preload: this._preload.bind(this), create: this._create.bind(this) }, true);
		this.loader.setCore(this._engine);
		this.factory = new CORE.render.AssetFactory(this._engine);
	},

	toString: function() {
		return "CORE.render.Render";
	},

	_preload: function() {
	    if (this.loader.existPreload()) {
	        this.loader.startPreload();
	    }
	},	

	_create: function() {
		this.eventDispatcher.sendMessage(this.START_APP_MESSAGE, {});
	},

	setMinWidth: function(value) {
        this._engine.scale.minWidth = value;
    },    

    setMinHeight: function(value) {
        this._engine.scale.minHeight = value;
    },
    
    setMaxWidth: function(value) {
        this._engine.scale.maxWidth = value;
    },    

    setMaxHeight: function(value) {
        this._engine.scale.maxHeight = value;
    },
    
    setScaleMode: function(mode) {
        this._engine.scale.scaleMode = mode || this.SHOW_ALL;
    },

    setFullScreenScaleMode: function(mode) {
        this._engine.scale.scaleMode = mode || this.SHOW_ALL;
    },

    setPageAlignHorizontally: function(value) {
        this._engine.scale.pageAlignHorizontally = value;
    },

    setPageAlignVertically: function(value) {
        this._engine.scale.pageAlignVertically = value;
    },

    setPaused: function(value) {
        this._engine.paused = value;
    },

    getScale: function() {
        return this._engine.scale;        
    },
	
    getWorld: function() {
        return this._engine.world;        
    }
});