//StateManager
var CORE = CORE || {};
CORE.state = CORE.state || {};

CORE.state.StateManager = ring.create({
	currentState: null,

	constructor: function() {
    	this.create();
	},

	create: function() {
	
	},

	update: function() {
	    if (this.currentState != null) {
	        this.currentState.update();
	    }
	},

	destroy: function() {
	    this.destroyCurrentState();
	},

	destroyCurrentState: function() {
	    if (this.currentState != null) {
	        this.currentState.destroy();
	    }
	},

	changeState: function(Class, params) {
	    this.destroyCurrentState();

	    if (Class != null) {
	        this.currentState = new Class(params);
	        this.currentState.create();
	        return this.currentState;
	    }
	    return null;
	},
	
	toString: function() {
		return "CORE.state.StateManager";
	}
});