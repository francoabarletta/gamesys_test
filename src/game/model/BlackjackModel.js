var GAME = GAME || {};
GAME.model = GAME.model || {};

GAME.model.BlackjackModel = ring.create({
	
	START_CARDS: 2,
	cardsModel: null,
	
	player: null,
	croupier: null,
	
	constructor: function() {
	
		this.player = new GAME.model.Player();
		this.croupier = new GAME.model.Player();
		
		this.cardsModel = new GAME.model.Cards();
		
		this.cardsModel.shuffle();
		
		//give 2 cards for each player
		for(var i = 0; i<this.START_CARDS; i++) {
			this.player.addCard(this.cardsModel.popCard());
			this.croupier.addCard(this.cardsModel.popCard());		
		}
	},
	
	playCroupier: function() {
		//Basic IA
		while(!this.croupier.hasFinished()) {
			var stand = this.player.getCardsValue() < this.croupier.getCardsValue();
			
			if(stand) {
				this.croupier.stand();
			} else {
				this.croupier.addCard(this.cardsModel.popCard());		
			}
		}
	},
	
	playerWins: function() {
		
		return this.croupier.isBusted() || 
			(this.croupier.getCardsValue() < this.player.getCardsValue() && !this.player.isBusted());

	},
	
	hit() {
		this.player.addCard(this.cardsModel.popCard());
	},		
	
	stand() {
		this.player.stand();
	}	
	
});