var GAME = GAME || {};
GAME.model = GAME.model || {};

GAME.model.Cards = ring.create({
	
	//"HEARTS", "CLOVES", "SPADES", "DIAMONDS"]
	SYMBOLS: ["H", "C", "S", "D"],
	CARDS: ["A","2","3","4","5","6","7","8","9","10","J","Q","K"],
	CARDS_VALUES: [11, 2, 3, 4, 5, 6, 7, 8 , 9, 10 , 10, 10, 10],
	
	cardsArray: null,
	
	constructor : function() {
		this.cardsArray = [];
		this.buildCards();
	},
	
	buildCards: function() {
		for(var key in this.SYMBOLS) {
			for(var i=0; i < this.CARDS.length; i++) {
				var symbol = this.SYMBOLS[key] + "_" + this.CARDS[i];
				console.log(symbol);
				this.cardsArray.push(new GAME.model.Card(symbol, this.CARDS_VALUES[i]));
			}
		}
	},
	
	shuffle: function() {
		this.cardsArray = _.shuffle(this.cardsArray);
	},
	
	popCard: function() {
		var card = this.cardsArray.pop();
		console.log(card);
		return card;
	}
});