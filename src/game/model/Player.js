var GAME = GAME || {};
GAME.model = GAME.model || {};


GAME.model.Player = ring.create({
	
	cards: null,
	stood: false,
	
	constructor: function() {
		this.cards = [];
	},
	
	addCard: function(cardModel) {
		this.cards.push(cardModel);
	},
	
	getCardsValue: function() {
		var cardModel;
		var value = 0;
		for(var i = 0; i < this.cards.length; i++) {
			cardModel = this.cards[i];
			value += cardModel.value;
		}
		return value;
	},
	
	isBusted: function() {
		return this.getCardsValue() > Model.config.maxCardValue;	
	},
	
	hasFinished: function() {
		return this.stood || this.isBusted();	
	},	
	
	stand: function() {
		this.stood = true;	
	}
});