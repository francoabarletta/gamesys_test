var GAME = GAME || {};
GAME.state = GAME.state || {};

GAME.state.State = ring.create({
	
	stateId: null,
	enterState: null,
	exitState: null,
	updateState: null,
	scope: null,
	
	constructor: function(stateId, scope, enterState, exitState, updateState) 
	{
		this.stateId = stateId;
		this.enterState = enterState;
		this.exitState = exitState;
		this.updateState = updateState;
		this.scope = scope;
	}
});