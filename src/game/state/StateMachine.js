var GAME = GAME || {};
GAME.state = GAME.state || {};

GAME.state.StateMachine = ring.create({
		_currentStateId: null,
		_statesByType: null,
		
		constructor: function () {
			this._statesByType = {};
		},
	
		addState: function(state) 
		{
			this._statesByType[state.stateId] = state;
		},
		
		getCurrentState: function() {
			return this._statesByType[this._currentStateId];
		},
		
		switchStateByID: function(stateId) {
			var state = this.getCurrentState();
			if (state && state.exitState != null) {
				state.exitState.call(state.scope);
			}
			this._currentStateId = stateId;
			state = this.getCurrentState();
			if (state && state.enterState != null) {
				state.enterState.call(state.scope);
			}
		},
		
		update: function() {
			var state = this.getCurrentState();
			var updateStateFunction = state? state.updateState : null;
			if (updateStateFunction != null) {
				state.updateState.call(state.scope);
			}
		},
		
		destroy: function () {

			for (var key in this._statesByType) {
				delete this._statesByType[key];
			}
			this._statesByType = null;
			this._currentStateId = null;

		}
});