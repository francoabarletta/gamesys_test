var GAME = GAME || {};
GAME.views = GAME.views || {};

GAME.views.AbstractViewState = ring.create({

	view: null,
	
	constructor: function() {
	},
	
	create: function() {
		this.view = Model.render.factory.createGroup();
		Model.render.getWorld().add(this.view);
	},
	
	update: function() {
		
	},
	
	destroy: function() {
		Model.render.getWorld().remove(this.view);
		if(this.view) this.view.destroy();
		this.view = null;
	}
});