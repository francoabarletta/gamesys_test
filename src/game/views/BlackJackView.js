var GAME = GAME || {};
GAME.views = GAME.views || {};

GAME.views.BlackJackView = ring.create([GAME.views.AbstractViewState], {

		SHOW_CARDS: "SHOW_CARDS",
		STAND_OR_HIT: "STAND_OR_HIT",
		CHECK_NEXT_STATE: "CHECK_NEXT_STATE",
		PLAY_CROUPIER: "PLAY_CROUPIER",
		BUSTED: "BUSTED",
		SHOW_WINNER: "SHOW_WINNER",
		
		sm: null,
		blackjackModel:null,
		
		constructor: function() {
		
		},
			
		create: function() {
			this.$super();
			//init model
			this.blackjackModel = new GAME.model.BlackjackModel();
			this._buildView();
			
			this.sm = new GAME.state.StateMachine();
			//(stateId, scope, enterState, exitState, updateState) 
			this.sm.addState(new GAME.state.State(this.SHOW_CARDS, this, this.enterShowCardsState, this.exitBetState));
			this.sm.addState(new GAME.state.State(this.STAND_OR_HIT, this, this.enterStandOrHitState,  this.exitStandOrHitState));
			
			this.sm.addState(new GAME.state.State(this.CHECK_NEXT_STATE, this, this.enterCheckNextState));
			
			this.sm.addState(new GAME.state.State(this.PLAY_CROUPIER, this, this.enterPlayCroupierState));
			this.sm.addState(new GAME.state.State(this.BUSTED, this, this.enterBustedState));
			this.sm.addState(new GAME.state.State(this.SHOW_WINNER, this, this.enterShowWinnerState));
			
			this.sm.switchStateByID(this.SHOW_CARDS);
		},
		
		//SHOW_CARDS
		enterShowCardsState: function() {
			console.log("Player ::" + this.blackjackModel.player.getCardsValue());
			console.log("Croupier 1st card ::" + this.blackjackModel.croupier.cards[0].value);
			
			this.sm.switchStateByID(this.CHECK_NEXT_STATE);

		},
				
				
		//DEAL_OR_HIT
		enterStandOrHitState: function() {
			this.buttonsGroup.visble = true;

		},
		
		exitStandOrHitState: function() {
			this.buttonsGroup.visble = false;

		},
		
		//CHECK_NEXT_STATE
		enterCheckNextState: function() {
			var nextState;
			if( this.blackjackModel.player.hasFinished()) {
				nextState = this.blackjackModel.player.isBusted() ? this.BUSTED : this.PLAY_CROUPIER;
			} else {
				nextState = this.STAND_OR_HIT;
			}
			
			this.sm.switchStateByID(nextState);
		},
		
		//PLAY CROUPIER
		enterPlayCroupierState: function() {
			this.blackjackModel.playCroupier();
			
			this.sm.switchStateByID(this.SHOW_WINNER);
		},		
		
		enterBustedState: function() {
			
			console.log("BUSTED");
			this.sm.switchStateByID(this.SHOW_WINNER);

		},
				
		enterShowWinnerState: function() {
			var playerWin = this.blackjackModel.playerWins();
			console.log("Croupier values:: " + this.blackjackModel.croupier.getCardsValue());
			console.log("Player values:: " + this.blackjackModel.player.getCardsValue());
			
			console.log("Player win:: " + playerWin);
			
			if(playerWin) {
				Model.currentBalance += (Model.betValue * 2);
			}
			

			//TODO: if balance is over goto GAME OVER
			Model.stateManager.changeState(GAME.views.SelectBetState);

		},
		
		_buildView: function() {
			
			this.buttonsGroup = Model.render.factory.createGroup();
			this.buttonsGroup.anchor  = new Phaser.Point(0.5,0.5);
			this.buttonsGroup.x = Model.config.stageWidth * 0.5;
			this.buttonsGroup.y = Model.config.stageHeight * 0.5;
			
			this.buttonsGroup.visble = false;
			this.view.addChild(this.buttonsGroup);
			
			//stand button
			this.standButton = Model.render.factory.createButton("stand", [0,0,0,0], {scope: this, up: this._handleStandClick});
			this.standButton.anchor  = new Phaser.Point(0.5,0.5);
			this.standButton.x = 75;
			this.buttonsGroup.addChild(this.standButton);			
			
			//hit button
			this.hitButton = Model.render.factory.createButton("hit", [0,0,0,0], {scope: this, up: this._handleHitClick});
			this.hitButton.anchor  = new Phaser.Point(0.5,0.5);
			this.hitButton.x = - 75;
			
			this.buttonsGroup.addChild(this.hitButton);

			//Text
			this.balanceText = Model.render.factory.createText();
			this.view.addChild(this.balanceText);
			
			this.balanceText.text = "Balance : " + Model.currentBalance;
		},
		
		_handleStandClick: function() {
			this.blackjackModel.stand();
			this.sm.switchStateByID(this.CHECK_NEXT_STATE);

		},
		
		_handleHitClick: function() {
			this.blackjackModel.hit();
			this.sm.switchStateByID(this.SHOW_CARDS);
		},
		
				
		destroy: function() {
			if(this.buttonsGroup) this.buttonsGroup.destroy();
			this.buttonsGroup = null;
			this.$super();
		}
});