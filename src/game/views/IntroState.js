var GAME = GAME || {};
GAME.views = GAME.views || {};

GAME.views.IntroState = ring.create([GAME.views.AbstractViewState], {

		constructor: function() {
		
		},
		
		create: function() {
			this.$super();
			var logo = Model.render.factory.createImage("logo");
			this.view.addChild(logo);
			
			//center
			logo.x = (Model.config.stageWidth - logo.width) * 0.5;
			logo.y = (Model.config.stageHeight - logo.height) * 0.5;
			
			//on click switch to other state
			var playButton = Model.render.factory.createButton("play", [0,0,0,0], {scope: this, up: this._handleClick});
			playButton.x = (Model.config.stageWidth - playButton.width) * 0.5;
			playButton.y = logo.y + logo.height + 10;
			this.view.addChild(playButton);

		},
		
		_handleClick: function() {
			Model.stateManager.changeState(GAME.views.SelectBetState);
		},
		
		update: function() {
			this.$super();

		},
		
		destroy: function() {
			this.$super();
		}
		
	});