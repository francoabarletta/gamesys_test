var GAME = GAME || {};
GAME.views = GAME.views || {};

GAME.views.SelectBetState = ring.create([GAME.views.AbstractViewState], {
		
		defaultStepValue: 25,
		stepBetValue: null,
		betSelectorGroup: null,
		betValue: 0,
		
		_coinSound: null,
		_cashSound: null,
		
		create: function() {
			this.$super();
			this.stepBetValue = Model.config.stepBetValue || this.defaultStepValue;
			this._buildView();
		},
		
		_buildView: function() {
			
			
			this._coinSound = Model.render.factory.createAudio("coin");
			this._cashSound = Model.render.factory.createAudio("cash");
			//create container
			this.betSelectorGroup = Model.render.factory.createGroup();
			this.betSelectorGroup.visble = false;
			
			this.view.addChild(this.betSelectorGroup);
			
			//decrease 
			this.leftArrowButton = Model.render.factory.createButton("leftArrow", [0,0,0,0], {scope: this, up: this._decreaseBet});
			this.leftArrowButton.x = 0;
			this.betSelectorGroup.addChild(this.leftArrowButton);
	
			//increase 
			this.rightArrowButton = Model.render.factory.createButton("rightArrow", [0,0,0,0], {scope: this, up: this._increaseBet});
			this.betSelectorGroup.addChild(this.rightArrowButton);
			this.rightArrowButton.x = 200;

			//text
			this.betValueText = Model.render.factory.createText();
			this.betSelectorGroup.addChild(this.betValueText);
			this.betValueText.align = "center";
			this.betValueText.anchor = new Phaser.Point(0.5,0.5);
			this.betValueText.x = this.betSelectorGroup.width * 0.5;
			this.betValueText.y = this.betSelectorGroup.height * 0.5;
			
			//bet button
			this.betButton = Model.render.factory.createButton("bet", [0,0,0,0], {scope: this, up: this._acceptBetValue});
			this.betButton.y = this.betSelectorGroup.height + 5;
			this.betButton.x = (this.betSelectorGroup.width - this.betButton.width)*0.5;
			this.betSelectorGroup.addChild(this.betButton);
			
			//center
			this.betSelectorGroup.x = (Model.config.stageWidth - this.betSelectorGroup.width) * 0.5;
			this.betSelectorGroup.y = (Model.config.stageHeight - this.betSelectorGroup.height) * 0.5;
			this._refreshBetValue();

			//Text
			this.balanceText = Model.render.factory.createText();
			this.view.addChild(this.balanceText);
			
			this.balanceText.text = "Balance : " + Model.currentBalance;
		},
	
		_refreshBetValue: function() {
			this.betValueText.text = this.betValue.toString();
		},
		
		_increaseBet: function() {
			if(this._canIncreaseBet()) {
				this.betValue += this.stepBetValue;
				this._coinSound.play();
			}	
			this._refreshBetValue();
		},

		_decreaseBet: function() {
			if(this._canDecreaseBet()) {
				this.betValue -= this.stepBetValue;
				this._coinSound.play();
			}
			this._refreshBetValue();
		},
		
		_acceptBetValue: function() {
			
			Model.currentBalance -= this.betValue;
			Model.betValue = this.betValue;
			this._cashSound.play();

			Model.stateManager.changeState(GAME.views.BlackJackView);
		},
		
		_canIncreaseBet: function() {
			return (this.betValue + this.stepBetValue) <= Model.currentBalance;
		},		
		
		_canDecreaseBet: function() {
			return this.betValue > this.stepBetValue;
		},
		
		update: function() {
			this.$super();

		},
		
		destroy: function() {
			if(this.betSelectorGroup) this.betSelectorGroup.destroy();
			this.betSelectorGroup = null;
			this.$super();
		}
		

	});